# itweather APP

> Desafio Itflex

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

### BUGS e OBS

> Component autocomplete do vue-material fix: https://github.com/vuematerial/vue-material/issues/1740

> Em geral https://github.com/vuematerial ainda esta em uma versão muito nova e com muitos bugs em seus componentes, recomendo usar https://vuetifyjs.com/pt-BR/

> 16 days/daily forecast API não é free https://openweathermap.org/price, utilizada versão da API 5 days/3 hour forecast API

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
